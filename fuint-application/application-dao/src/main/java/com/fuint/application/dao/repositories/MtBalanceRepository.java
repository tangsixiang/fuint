package com.fuint.application.dao.repositories;

import com.fuint.base.dao.BaseRepository;
import org.springframework.stereotype.Repository;
import com.fuint.application.dao.entities.MtBalance;

/**
* mt_balance Repository
* Created by FSQ
* Contact wx fsq_better
* Site https://www.fuint.cn
*/ 
@Repository 
public interface MtBalanceRepository extends BaseRepository<MtBalance, Integer> {
}

